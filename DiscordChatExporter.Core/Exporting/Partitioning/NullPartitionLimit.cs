﻿namespace DiscordChatExporter.Core.Exporting.Partitioning
{
    public class NullPartitionLimit : PartitionLimit
    {
        public override bool IsReached(long messagesWritten, long bytesWritten) => false;
    }
}