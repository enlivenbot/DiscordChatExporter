﻿namespace DiscordChatExporter.Core.Exporting.Filtering
{
    public enum MessageContentMatchKind
    {
        Link,
        Embed,
        File,
        Video,
        Image,
        Sound
    }
}