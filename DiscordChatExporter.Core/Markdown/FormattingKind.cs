﻿namespace DiscordChatExporter.Core.Markdown
{
    public enum FormattingKind
    {
        Bold,
        Italic,
        Underline,
        Strikethrough,
        Spoiler,
        Quote
    }
}