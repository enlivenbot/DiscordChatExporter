﻿namespace DiscordChatExporter.Core.Markdown
{
    public enum MentionKind
    {
        Meta,
        User,
        Channel,
        Role
    }
}